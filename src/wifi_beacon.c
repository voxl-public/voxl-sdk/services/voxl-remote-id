/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <unistd.h> // sleep()
#include <semaphore.h>

// ModalAI includes
#include <modal_journal.h>

// Local Includes
#include "wifi_beacon.h"
#include "opendroneid.h"
#include "hostapd_interface.h"

#define WIFI_BEACON_HEADER_SIZE 7

// extern variables
extern sem_t hostapd_sem;
extern struct wpa_ctrl *ctrl_conn;

// -----------------------------------------------------------------------------------------------------------------------------
// Convert a single uint8_t to two chars representing the value in ASCII format
// 0 - 9 => 0x30 - 0x39, A - F => 0x41 - 0x46
// -----------------------------------------------------------------------------------------------------------------------------
static void _uchar_to_ascii(char *out, uint8_t in) {
    if (!out)
        return;

    unsigned char high = (in & 0xF0) >> 4;
    if (high < 0xA)
        *out++ = (char) (0x30 + high);
    else
        *out++ = (char) (0x41 + high - 0xA);
    unsigned char low = in & 0x0F;
    if (low < 0xA)
        *out = (char) (0x30 + low);
    else
        *out = (char) (0x41 + low - 0xA);
}


// -----------------------------------------------------------------------------------------------------------------------------
// send beacon
// -----------------------------------------------------------------------------------------------------------------------------
static void _send_update_beacon() {
    char *cmd[] = { "update_beacon" };
    wpa_request(ctrl_conn, sizeof(cmd)/sizeof(cmd[0]), cmd);
    sem_wait(&hostapd_sem);
}


// -----------------------------------------------------------------------------------------------------------------------------
// The header for WiFi Beacons, when specifying the data for vendor specific information elements,
// consists of the following parts:
//     dd = Indicates to hostapd that the following data is hexadecimal
//     1E = The length of the data (30 bytes)
//     FA, 0B, BC = The OUI reserved for ASD-STAN
//     0D = The indicator within the ASD-STAN OUI address space indicating Direct Remote ID
//     xx = 8-bit message counter starting at 0x00 and wrapping around at 0xFF
// -----------------------------------------------------------------------------------------------------------------------------
void send_beacon_message_pack(struct ODID_MessagePack_encoded *pack_enc, uint8_t msg_counter) {
    char *cmd[] = { "set", "vendor_elements", "dd1EFA0BBC0D00" };

    // The buffers cmd points to are in read-only memory. Create a writable buffer
    char data[2*(WIFI_BEACON_HEADER_SIZE + 3 + ODID_PACK_MAX_MESSAGES*ODID_MESSAGE_SIZE)] = { 0 };
    memcpy(data, cmd[2], 2*WIFI_BEACON_HEADER_SIZE);
    cmd[2] = data;

    // Update the data length
    int amount = pack_enc->MsgPackSize;
    _uchar_to_ascii(&data[2], (WIFI_BEACON_HEADER_SIZE - 2) + 3 + amount*ODID_MESSAGE_SIZE);

    // Insert the message counter
    _uchar_to_ascii(&data[12], msg_counter);

    // Insert the encoded message data
    for (int i = 0; i < 3 + amount*ODID_MESSAGE_SIZE; i++)
        _uchar_to_ascii(&data[2*(WIFI_BEACON_HEADER_SIZE + i)], ((char *) pack_enc)[i]);

    
    M_DEBUG("DEBUG:   Sending message pack: %s\n", cmd[0]);
    
    wpa_request(ctrl_conn, sizeof(cmd)/sizeof(cmd[0]), cmd);
    sem_wait(&hostapd_sem);
    sleep(1);

    _send_update_beacon();
    sleep(1);
}


// -----------------------------------------------------------------------------------------------------------------------------
// cleanup
// -----------------------------------------------------------------------------------------------------------------------------
void send_quit() {
    char *cmd[] = { "quit" };
    wpa_request(ctrl_conn, sizeof(cmd)/sizeof(cmd[0]), cmd);
    sem_wait(&hostapd_sem);
}