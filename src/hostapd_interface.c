/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>

// ModalAI includes
#include <modal_journal.h>

// WPA includes
#include "utils/eloop.h"
#include "utils/edit.h"
#include "utils/common.h"
#include "common/cli.h"

// local includes
#include "voxl_remote_id.h"
#include "hostapd_interface.h"

// extern variables
extern sem_t hostapd_sem;

// Global control interfaces
struct wpa_ctrl *ctrl_conn;
static char *ctrl_ifname = NULL;
static const char *ctrl_iface_dir = "";
static const char *ctrl_iface_dir_m0054 = "/data/misc/wifi/hostapd";
static const char *ctrl_iface_dir_m0052 = "/run/hostapd";

// Global misc
static const char *client_socket_dir = NULL;
static const char *action_file = NULL;
static const char *pid_file = NULL;

static int hostapd_cli_attached = 0;
static int event_handler_registered = 0;
static int ping_interval = 5;

// Struct cli_txt_entry
static DEFINE_DL_LIST(stations);

// Command handler struct
struct hostapd_cli_cmd {
	const char *cmd;
	int (*handler)(struct wpa_ctrl *ctrl, int argc, char *argv[]);
	char ** (*completion)(const char *str, int pos);
	const char *usage;
};


// -----------------------------------------------------------------------------------------------------------------------------
// Cleanup functions
// -----------------------------------------------------------------------------------------------------------------------------
static void _unregister_event_handler(struct wpa_ctrl *ctrl) {
	if (!ctrl_conn)
		return;

	if (event_handler_registered) {
		eloop_unregister_read_sock(wpa_ctrl_get_fd(ctrl));
		event_handler_registered = 0;
	}
}

static void _hostapd_cli_close_connection(void) {
	if (ctrl_conn == NULL)
		return;

	_unregister_event_handler(ctrl_conn);
	if (hostapd_cli_attached) {
		wpa_ctrl_detach(ctrl_conn);
		hostapd_cli_attached = 0;
	}

	wpa_ctrl_close(ctrl_conn);
	ctrl_conn = NULL;
}

static void _hostapd_cli_cleanup(void) {
	_hostapd_cli_close_connection();
	if (pid_file)
		os_daemonize_terminate(pid_file);

	os_program_deinit();
}

static void _hostapd_cli_eloop_terminate(int sig, void *signal_ctx) {
	eloop_terminate();
}


// -----------------------------------------------------------------------------------------------------------------------------
// 
// -----------------------------------------------------------------------------------------------------------------------------
static void _cli_event(const char *str) {
	const char *start, *s;

	start = os_strchr(str, '>');
	if (start == NULL)
		return;

	start++;

	if (str_starts(start, AP_STA_CONNECTED)) {
		s = os_strchr(start, ' ');
		if (s == NULL)
			return;
		cli_txt_list_add(&stations, s + 1);
		return;
	}

	if (str_starts(start, AP_STA_DISCONNECTED)) {
		s = os_strchr(start, ' ');
		if (s == NULL)
			return;
		cli_txt_list_del_addr(&stations, s + 1);
		return;
	}
}


// -----------------------------------------------------------------------------------------------------------------------------
// wpa status callback
// -----------------------------------------------------------------------------------------------------------------------------
static void _hostapd_cli_msg_cb(char *msg, size_t len) {
	_cli_event(msg);
	
	M_DEBUG("DEBUG:   printing hostapd client message:\n");
	M_DEBUG("DEBUG:   %s\n", msg);
}


static int _wpa_ctrl_command_sta(struct wpa_ctrl *ctrl, const char *cmd, char *addr, size_t addr_len, __attribute__((unused)) int print) {
	char buf[4096], *pos;
	size_t len;
	int ret;

	if (ctrl_conn == NULL) {
		M_ERROR("Not connected to hostapd, command dropped\n");
		return -1;
	}

	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(ctrl, cmd, strlen(cmd), buf, &len, _hostapd_cli_msg_cb);
	if (ret == -2) {
		M_ERROR("Command timed out: %s\n", cmd);
		return -2;
	} else if (ret < 0) {
		M_ERROR("Command failed: %s\n", cmd);
		return -1;
	}

	buf[len] = '\0';
	M_DEBUG("DEBUG:   %s\n", buf);
	if (memcmp(buf, "FAIL", 4) == 0)
		return -1;

	pos = buf;
	while (*pos != '\0' && *pos != '\n')
		pos++;
	*pos = '\0';
	os_strlcpy(addr, buf, addr_len);

	return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Utility functions
// -----------------------------------------------------------------------------------------------------------------------------
static int _wpa_ctrl_command(struct wpa_ctrl *ctrl, const char *cmd, __attribute__((unused)) int print) {
	char buf[4096];
	size_t len;
	int ret = 0;

	if (ctrl_conn == NULL) {
		M_ERROR("Not connected to hostapd, command dropped\n");
		return -1;
	}
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(ctrl, cmd, strlen(cmd), buf, &len, _hostapd_cli_msg_cb);

	// Indicate the command has been processed to the end
	sem_post(&hostapd_sem);

	if (ret == -2) {
		M_ERROR("Command timed out: %s\n", cmd);
		goto ERROR;
	} else if (ret < 0) {
		M_ERROR("Command failed: %s\n", cmd);
		ret = -1;
		goto ERROR;
	}

	buf[len] = '\0';
	M_DEBUG("DEBUG:   %s", buf);
	if (memcmp(buf, "FAIL", 4) == 0) {
		ret = -3;
		goto ERROR;
	}

	return ret;

ERROR:
	M_ERROR("Ping failed: %d\n", ret);
	set_service_state_to_not_ready(HOSTAPD);
	return ret;
}

static void _hostapd_cli_receive(int sock, void *eloop_ctx, void *sock_ctx) {
	if (ctrl_conn == NULL)
		return;

	while (wpa_ctrl_pending(ctrl_conn)) {
		char buf[4096];
		size_t len = sizeof(buf) - 1;
		if (wpa_ctrl_recv(ctrl_conn, buf, &len) == 0) {
			buf[len] = '\0';

			_cli_event(buf);
			M_DEBUG("DEBUG:   %s\n", buf);
		} else {
			M_ERROR("Could not read pending message\n");
			break;
		}
	}
}

static void _update_stations(struct wpa_ctrl *ctrl) {
	char addr[32], cmd[64];

	if (!ctrl)
		return;

	cli_txt_list_flush(&stations);

	if (_wpa_ctrl_command_sta(ctrl, "STA-FIRST", addr, sizeof(addr), 0)) return;

	do {
		if (os_strcmp(addr, "") != 0)
			cli_txt_list_add(&stations, addr);

		os_snprintf(cmd, sizeof(cmd), "STA-NEXT %s", addr);
	} while (_wpa_ctrl_command_sta(ctrl, cmd, addr, sizeof(addr), 0) == 0);
}

static void _register_event_handler(struct wpa_ctrl *ctrl) {
	if (!ctrl_conn)
		return;

	// Only register since we're in interactive mode	
	event_handler_registered = !eloop_register_read_sock(wpa_ctrl_get_fd(ctrl), _hostapd_cli_receive, NULL, NULL);
}

static int _hostapd_cli_exec(const char *program, const char *arg1, const char *arg2) {
	char *arg;
	size_t len;
	int res;

	len = os_strlen(arg1) + os_strlen(arg2) + 2;
	arg = os_malloc(len);
	if (arg == NULL)
		return -1;

	os_snprintf(arg, len, "%s %s", arg1, arg2);
	res = os_exec(program, arg, 1);
	os_free(arg);

	return res;
}


static void _hostapd_cli_action_process(char *msg, size_t len) {
	const char *pos;

	pos = msg;
	if (*pos == '<') {
		pos = os_strchr(pos, '>');
		if (pos)
			pos++;
		else
			pos = msg;
	}

	_hostapd_cli_exec(action_file, ctrl_ifname, pos);
}

static void _hostapd_cli_recv_pending(struct wpa_ctrl *ctrl, int in_read, int action_monitor) {
	if (ctrl_conn == NULL)
		return;
	while (wpa_ctrl_pending(ctrl)) {
		char buf[4096];
		size_t len = sizeof(buf) - 1;
		if (wpa_ctrl_recv(ctrl, buf, &len) == 0) {
			buf[len] = '\0';
			if (action_monitor)
				_hostapd_cli_action_process(buf, len);
			else {
				_cli_event(buf);
				M_DEBUG("DEBUG:   %s\n", buf);
			}
		} else {
			M_ERROR("Could not read pending message.\n");
			break;
		}
	}
}


// -----------------------------------------------------------------------------------------------------------------------------
// Init functions
// -----------------------------------------------------------------------------------------------------------------------------
static struct wpa_ctrl *_hostapd_cli_open_connection(const char *ifname) {
	char *cfile;
	int flen;

	if (ifname == NULL)
		return NULL;

	flen = strlen(ctrl_iface_dir) + strlen(ifname) + 2;
	cfile = malloc(flen);
	if (cfile == NULL)
		return NULL;

	snprintf(cfile, flen, "%s/%s", ctrl_iface_dir, ifname);

	if (client_socket_dir && client_socket_dir[0] && access(client_socket_dir, F_OK) < 0) {
		perror(client_socket_dir);
		free(cfile);
		return NULL;
	}

	ctrl_conn = wpa_ctrl_open2(cfile, client_socket_dir);
	free(cfile);
	return ctrl_conn;
}

static int _hostapd_cli_reconnect(const char *ifname) {
	char *next_ctrl_ifname;

	_hostapd_cli_close_connection();

	if (!ifname)
		return -1;

	next_ctrl_ifname = os_strdup(ifname);
	os_free(ctrl_ifname);
	ctrl_ifname = next_ctrl_ifname;
	if (!ctrl_ifname)
		return -1;

	ctrl_conn = _hostapd_cli_open_connection(ctrl_ifname);
	if (!ctrl_conn)
		return -1;

	if (!action_file)
		return 0;

	if (wpa_ctrl_attach(ctrl_conn) == 0) {
		hostapd_cli_attached = 1;
		_register_event_handler(ctrl_conn);
		_update_stations(ctrl_conn);
	} else 
		M_WARN("Failed to attach to hostapd.\n");

	return 0;
}

static void _hostapd_cli_ping(void *eloop_ctx, void *timeout_ctx) {
	if (ctrl_conn && _wpa_ctrl_command(ctrl_conn, "PING", 0)) {
		M_ERROR("Connection to hostapd lost, trying to reconnect\n");
		_hostapd_cli_close_connection();
	}
	if (!ctrl_conn && _hostapd_cli_reconnect(ctrl_ifname) == 0) {
		M_PRINT("INFO:    Connection to hostapd re-established\n");
		set_service_state_to_ready(HOSTAPD);
	}
	if (ctrl_conn)
		_hostapd_cli_recv_pending(ctrl_conn, 1, 0);

	eloop_register_timeout(ping_interval, 0, _hostapd_cli_ping, NULL, NULL);
}

static void _hostapd_cli_interactive() {
	eloop_register_signal_terminate(_hostapd_cli_eloop_terminate, NULL);
	eloop_register_timeout(ping_interval, 0, _hostapd_cli_ping, NULL, NULL);

	eloop_run();

	cli_txt_list_flush(&stations);
	edit_deinit(NULL, NULL);
	eloop_cancel_timeout(_hostapd_cli_ping, NULL, NULL);
}


// -----------------------------------------------------------------------------------------------------------------------------
// Hostapd client commands
// -----------------------------------------------------------------------------------------------------------------------------
static int _hostapd_cli_cmd_set(struct wpa_ctrl *ctrl, int argc, char *argv[]) {
	char cmd[2048];
	int res;

	if (argc != 2) {
		M_ERROR("Invalid SET command: needs two arguments (variable name and value)\n");
		return -1;
	}

	res = os_snprintf(cmd, sizeof(cmd), "SET %s %s", argv[0], argv[1]);
	if (os_snprintf_error(sizeof(cmd), res)) {
		M_ERROR("Too long SET command.\n");
		return -1;
	}

	M_DEBUG("DEBUG:   %s\n", cmd);
	M_DEBUG("DEBUG:   Result:\n");
		
	return _wpa_ctrl_command(ctrl, cmd, 0);
}

static char **_hostapd_complete_set(const char *str, int pos) {
	int arg = get_cmd_arg_num(str, pos);
	const char *fields[] = {
		"deny_mac_file", "accept_mac_file",
	};
	int i, num_fields = ARRAY_SIZE(fields);

	if (arg == 1) {
		char **res;

		res = os_calloc(num_fields + 1, sizeof(char *));
		if (!res)
			return NULL;

		for (i = 0; i < num_fields; i++) {
			res[i] = os_strdup(fields[i]);
			if (!res[i])
				return res;
		}
		return res;
	}
	return NULL;
}

static int _hostapd_cli_cmd_update_beacon(struct wpa_ctrl *ctrl, int argc, char *argv[]) {
	M_DEBUG("DEBUG:   update_beacon. Result: \n");

	return _wpa_ctrl_command(ctrl, "UPDATE_BEACON", 0);
}

static int _hostapd_cli_cmd_quit(struct wpa_ctrl *ctrl, int argc, char *argv[]) {
	eloop_terminate();

	sem_post(&hostapd_sem);
	return 0;
}

static const struct hostapd_cli_cmd hostapd_cli_commands[] = {
	{ "set", _hostapd_cli_cmd_set, _hostapd_complete_set, "<name> <value> = set runtime variables" },
	{ "update_beacon", _hostapd_cli_cmd_update_beacon, NULL, "= update Beacon frame contents\n"},
	{ "quit", _hostapd_cli_cmd_quit, NULL, "= exit hostapd_cli" },
	{ NULL, NULL, NULL, NULL }
};


// -----------------------------------------------------------------------------------------------------------------------------
// Request command externally
// -----------------------------------------------------------------------------------------------------------------------------
void wpa_request(struct wpa_ctrl *ctrl, int argc, char *req_cmd[]) {
	const struct hostapd_cli_cmd *cmd, *match = NULL;
	int count;

	count = 0;
	cmd = hostapd_cli_commands;
	while (cmd->cmd) {
		if (strncasecmp(cmd->cmd, req_cmd[0], strlen(req_cmd[0])) == 0) {
			match = cmd;
			if (os_strcasecmp(cmd->cmd, req_cmd[0]) == 0) {
				/* we have an exact match */
				count = 1;
				break;
			}
			count++;
		}
		cmd++;
	}

	if (count > 1) {
		M_ERROR("Ambiguous command: %s", req_cmd[0]);
		M_ERROR("		possible commands:");
		cmd = hostapd_cli_commands;
		while (cmd->cmd) {
			if (strncasecmp(cmd->cmd, req_cmd[0], strlen(req_cmd[0])) ==
				0) {
				M_ERROR(" %s", cmd->cmd);
			}
			cmd++;
		}
		M_PRINT("\n");
	} else if (count == 0) {
		M_ERROR("Unknown command: %s\n", req_cmd[0]);
	} else {
		match->handler(ctrl, argc - 1, &req_cmd[1]);
	}
}


// -----------------------------------------------------------------------------------------------------------------------------
// Hostapd initialization thread
// -----------------------------------------------------------------------------------------------------------------------------
void *hostapd_init_thread(__attribute__((unused)) void* ctx) {
	if (eloop_init())
		return NULL;

	// attempt to connect to hostapd
	bool is_connected = false;
	while(!is_connected) {
		
		// get control interface
		if (ctrl_ifname == NULL) {
			struct dirent *dent;
			ctrl_iface_dir = ctrl_iface_dir_m0054;
			DIR *dir = opendir(ctrl_iface_dir);
			if (!dir) {
				// on M0052 target, we use this file
				ctrl_iface_dir = ctrl_iface_dir_m0052;
				dir = opendir(ctrl_iface_dir);
			}

			if (dir) {
				
				while ((dent = readdir(dir))) {
					if (os_strcmp(dent->d_name, ".") == 0 || os_strcmp(dent->d_name, "..") == 0)
						continue;

					M_DEBUG("DEBUG:   Selected interface '%s'\n", dent->d_name);
					
					ctrl_ifname = os_strdup(dent->d_name);
					break;
				}
				closedir(dir);
				M_PRINT("INFO:    Attempting to Connect to hostapd: %s/%s\n", ctrl_iface_dir, ctrl_ifname);
			}
		}

		// attempt to connect to hostapd interface
		_hostapd_cli_reconnect(ctrl_ifname);
		if (ctrl_conn) {
			is_connected = true;

			M_PRINT("INFO:    Connected to hostapd: %s/%s\n", ctrl_iface_dir, ctrl_ifname);

			break;
		}
		os_sleep(1, 0);
	}

	// Indicate that connection has been established
	sem_post(&hostapd_sem);

	// This will begin the interactive loop
	set_service_state_to_ready(HOSTAPD);
	_hostapd_cli_interactive();

	// cleanup
	set_service_state_to_not_ready(HOSTAPD);
	_unregister_event_handler(ctrl_conn);
	os_free(ctrl_ifname);
	eloop_destroy();
	_hostapd_cli_cleanup();

	return NULL;
}