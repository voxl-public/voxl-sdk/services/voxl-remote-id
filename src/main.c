/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <semaphore.h>

// Local includes
#include "opendroneid.h"
#include "config_file.h"
#include "wifi_beacon.h"
#include "voxl_remote_id.h"
#include "hostapd_interface.h"

// ModalAI includes
#include <c_library_v2/common/mavlink.h>
#include <modal_pipe_client.h>
#include <modal_pipe.h>
#include <modal_journal.h>


// System paths
#define SERIAL_NUMBER_PATH  "/sys/devices/soc0/serial_number"

// client name used when connecting to servers
#define PX4_MAVLINK_CH         0
#define PIPE_CLIENT_NAME      "voxl-remote-id"
#define PX4_MAVLINK_PIPE_NAME "mavlink_onboard"

#define VOXL_COMPID MAV_COMP_ID_ODID_TXRX_1
#define PX4_COMPID  MAV_COMP_ID_ODID_TXRX_1

#define MINIMUM(a,b) (((a)<(b))?(a):(b))
#define BASIC_ID_POS_ZERO 0
#define BASIC_ID_POS_ONE 1

#define MSG_INT_SEC_HEARTBEAT_MSG 1

// semaphore for hostapd communication
sem_t hostapd_sem;

// State tracking
static int hostapd_state = 0;
static int mavlink_state = 0;

// By default our operator location will be from wherever we takeoff from
// If a gnss is detected, then it will switch to using that data 
static int use_loc_for_operator_id = 1;

// Mavlink message sysid tracker
static uint8_t _sysid = 0;
static uint8_t _fix_type = 0;
static uint8_t _remote_id_active = 0;

static struct ODID_UAS_Data uas_data;
static pthread_t hostapd_thread, beacon_thread, heartbeat_thread;


/// @brief CLI
static void _print_help_message() {
    M_PRINT("\nvoxl-remote-id is a service that broadcasts remoteID data through a wifi beacon.\n");
    M_PRINT("This service is meant to run as a systemd background service. However, for debug\n");
    M_PRINT("purposes it can be started from the command line manually with any of the following\n");
    M_PRINT("debug options.\n\n");
    M_PRINT("-d, --debug-level         Log debug level (Default 2)\n");
    M_PRINT("                      0 : Print verbose logs\n");
    M_PRINT("                      1 : Print >= debug logs\n");
    M_PRINT("                      2 : Print >= warning logs\n");
    M_PRINT("                      3 : Print only error logs\n");
    M_PRINT("-h, --help                Print this help message\n");
    M_PRINT("-c, --create-json         create factory configuration file and exit. This is meant\n");
    M_PRINT("                          to be used by voxl-configure-remote-id\n");

}

/// @brief parse CLI inputs
/// @param argc 
/// @param argv 
/// @return 
static int _parse_args(int argc, char* const argv[]) {
    int debug_level;
    
    static struct option long_options[] =
        {
            {"help",             no_argument,        0, 'h'},
            {"create-json",      no_argument,        0, 'c'},
            {"debug-level",      no_argument,        0, 'd'},
        };

    while (1) {
        int option_idx = 0;
        int c = getopt_long(argc, argv, "hd:c", long_options, &option_idx);

        if (c == -1)
            break; // Detect the end of the options.

        switch (c) {
        case 0:
            // for long args without short equivalent that just set a flag
            // nothing left to do so just break.
            if (long_options[option_idx].flag != 0)
                break;
            break;

        case 'h':
            _print_help_message();
            return -1;

        case 'c':
            generate_config_file();
            exit(0);

        case 'd':
            if (sscanf(optarg, "%d", &debug_level) != 1) {
                M_ERROR("Failed to parse debug level specified after -d flag\n");
                return -1;
            }

            if (debug_level >= PRINT || debug_level < VERBOSE) {
                M_ERROR("Invalid debug level specified: %d\n", debug_level);
                return -1;
            }

            M_JournalSetLevel((M_JournalLevel) debug_level);
            break;

        default:
            _print_help_message();
            return -1;
        }
    }

    return 0;
}

/// @brief Set of functions used to track the state of remoteid. For remoteid to be ready, it must know that hostapd
///        and mavlink are also both ready
/// @param service MAVLINK or HOSTAPD
void set_service_state_to_ready(remotid_service_t service) {
    int *service_state;

    if (service == MAVLINK) service_state = &mavlink_state;
    else if (service == HOSTAPD) service_state = &hostapd_state;
    else {
        M_ERROR("Invalid service type\n");
        return;
    }

    // no need to do anything if state was already set to ready
    if (*service_state == 1) return;
    else *service_state = 1;

    if (hostapd_state && mavlink_state) {
        M_PRINT("INFO:    Remote ID is in active mode\n");
        _remote_id_active = 1;
    }

}

/// @brief Set of functions used to track the state of remoteid. For remoteid to be ready, it must know that hostapd
///        and mavlink are also both ready
/// @param service MAVLINK or HOSTAPD
void set_service_state_to_not_ready(remotid_service_t service) {
    int *service_state;

    if (service == MAVLINK) service_state = &mavlink_state;
    else if (service == HOSTAPD) service_state = &hostapd_state;
    else{
        M_ERROR("Invalid service type\n");
        return;
    }

    // no need to do anything if state was already set to not ready
    if (*service_state == 0) return;
    else *service_state = 0;

    if (!hostapd_state || !mavlink_state) {
        M_PRINT("INFO:    Remote ID is not ready\n");
        _remote_id_active = 0;
    }
}


/// @brief Function to handle mavlink messages and store them in the proper uas struct
/// @param msg mavlink_open_drone_id_system_t
static void _handle_operator_location_drone_id(mavlink_message_t* msg) {
    mavlink_open_drone_id_system_t mav_operator_loc_id = { 0 };
    mavlink_msg_open_drone_id_system_decode(msg, &mav_operator_loc_id);

    uas_data.System.OperatorLocationType = (ODID_operator_location_type_t) mav_operator_loc_id.operator_location_type;
    uas_data.System.ClassificationType   = (ODID_classification_type_t) mav_operator_loc_id.classification_type;
    uas_data.System.OperatorLatitude     = (double) mav_operator_loc_id.operator_latitude / 1E7;
    uas_data.System.OperatorLongitude    = (double) mav_operator_loc_id.operator_longitude / 1E7;
    uas_data.System.AreaCount            = mav_operator_loc_id.area_count;
    uas_data.System.AreaRadius           = mav_operator_loc_id.area_radius;
    uas_data.System.AreaCeiling          = mav_operator_loc_id.area_ceiling;
    uas_data.System.AreaFloor            = mav_operator_loc_id.area_floor;
    uas_data.System.CategoryEU           = (ODID_category_EU_t) mav_operator_loc_id.category_eu;
    uas_data.System.ClassEU              = (ODID_class_EU_t) mav_operator_loc_id.class_eu;
    uas_data.System.OperatorAltitudeGeo  = mav_operator_loc_id.operator_altitude_geo;
    uas_data.System.Timestamp            = mav_operator_loc_id.timestamp;


    M_VERBOSE("VERBOSE: Operator Location Data:\n");
    M_VERBOSE("VERBOSE: Operator Location Type: %d\n"
            "Classification Type: %d\nLat/Lon: %.7f, %.7f\n"
            "Area Count, Radius, Ceiling, Floor: %d, %d, %.2f, %.2f\n"
            "Category EU: %d, Class EU: %d, Altitude: %.2f, Timestamp: %u\n",
            uas_data.System.OperatorLocationType, uas_data.System.ClassificationType,
            uas_data.System.OperatorLatitude, uas_data.System.OperatorLongitude,
            uas_data.System.AreaCount, uas_data.System.AreaRadius,
            (double) uas_data.System.AreaCeiling, (double) uas_data.System.AreaFloor,
            uas_data.System.CategoryEU, uas_data.System.ClassEU,
            (double) uas_data.System.OperatorAltitudeGeo, uas_data.System.Timestamp);
}

/// @brief Function to handle mavlink messages and store them in the proper uas struct
/// @param msg mavlink_open_drone_id_location_t
static void _handle_location_drone_id(mavlink_message_t* msg) {
    mavlink_open_drone_id_location_t mav_location_id = { 0 };
    mavlink_msg_open_drone_id_location_decode(msg, &mav_location_id);

    uas_data.Location.Status          = (ODID_status_t) mav_location_id.status;
    uas_data.Location.Direction       = (float) mav_location_id.direction / 100;
    uas_data.Location.SpeedHorizontal = (float) mav_location_id.speed_horizontal / 100;
    uas_data.Location.SpeedVertical   = (float) mav_location_id.speed_vertical / 100;
    uas_data.Location.Latitude        = (double) mav_location_id.latitude / 1E7;
    uas_data.Location.Longitude       = (double) mav_location_id.longitude / 1E7;
    uas_data.Location.AltitudeBaro    = mav_location_id.altitude_barometric;
    uas_data.Location.AltitudeGeo     = mav_location_id.altitude_geodetic;
    uas_data.Location.HeightType      = (ODID_Height_reference_t) mav_location_id.height_reference;
    uas_data.Location.Height          = mav_location_id.height;
    uas_data.Location.HorizAccuracy   = (ODID_Horizontal_accuracy_t) mav_location_id.horizontal_accuracy;
    uas_data.Location.VertAccuracy    = (ODID_Vertical_accuracy_t) mav_location_id.vertical_accuracy;
    uas_data.Location.BaroAccuracy    = (ODID_Vertical_accuracy_t) mav_location_id.barometer_accuracy;
    uas_data.Location.SpeedAccuracy   = (ODID_Speed_accuracy_t) mav_location_id.speed_accuracy;
    uas_data.Location.TSAccuracy      = (ODID_Timestamp_accuracy_t) mav_location_id.timestamp_accuracy;
    uas_data.Location.TimeStamp       = mav_location_id.timestamp;


    // Only update operator location when we are using the default takeoff location (MAVLINK_MSG_ID_OPEN_DRONE_ID_SYSTEM packet not preset)
    // and we're on the ground. Once we takeoff, our operator location will remain as the most recent ground location (takeoff location)
    if (use_loc_for_operator_id && uas_data.Location.Status == ODID_STATUS_GROUND ) {
        uas_data.System.OperatorLocationType = ODID_OPERATOR_LOCATION_TYPE_TAKEOFF;
        uas_data.System.ClassificationType   = ODID_CLASSIFICATION_TYPE_UNDECLARED;
        uas_data.System.OperatorLatitude     = uas_data.Location.Latitude;
        uas_data.System.OperatorLongitude    = uas_data.Location.Longitude;

        // Use default/unknown values here
        uas_data.System.AreaCount            = 1;
        uas_data.System.AreaRadius           = 0;
        uas_data.System.AreaCeiling          = -1000.f;
        uas_data.System.AreaFloor            = -1000.f;

        // Leave unused for now
        // uas_data.System.CategoryEU        = ODID_CATEGORY_EU_OPEN;
        // uas_data.System.ClassEU           = ODID_CLASS_EU_CLASS_1;

        uas_data.System.OperatorAltitudeGeo  = uas_data.Location.AltitudeGeo;
        uas_data.System.Timestamp            = uas_data.Location.TimeStamp; // TODO: This is currently incorrect, different timestamp format
    }


    M_VERBOSE("VERBOSE: Location Data:\n");
    M_VERBOSE("VERBOSE: Status: %d\nDirection: %.1f\nSpeedHori: %.2f\nSpeedVert: "\
            "%.2f\nLat/Lon: %.7f, %.7f\nAlt: Baro, Geo, Height above %s: %.2f, "\
            "%.2f, %.2f\nHoriz, Vert, Baro, Speed, TS Accuracy: %.1f, %.1f, %.1f, "\
            "%.1f, %.1f\nTimeStamp: %.2f\n",
            uas_data.Location.Status, (double) uas_data.Location.Direction,
            (double) uas_data.Location.SpeedHorizontal, 
            (double) uas_data.Location.SpeedVertical, uas_data.Location.Latitude,
            uas_data.Location.Longitude, uas_data.Location.HeightType ? "Ground" : "TakeOff",
            (double) uas_data.Location.AltitudeBaro, (double) uas_data.Location.AltitudeGeo,
            (double) uas_data.Location.Height,
            (double) decodeHorizontalAccuracy(uas_data.Location.HorizAccuracy),
            (double) decodeVerticalAccuracy(uas_data.Location.VertAccuracy),
            (double) decodeVerticalAccuracy(uas_data.Location.BaroAccuracy),
            (double) decodeSpeedAccuracy(uas_data.Location.SpeedAccuracy),
            (double) decodeTimestampAccuracy(uas_data.Location.TSAccuracy),
            (double) uas_data.Location.TimeStamp);
}

/// @brief parses data from the PX4 pipe
/// @param msg MAVLink message struct
static void _parse_px4_data(mavlink_message_t *msg) {

    // always monitor the sysid of px4 in case it changes which may happen
    // during setup and config
    if(_sysid != msg->sysid){
        _sysid = msg->sysid;
        M_PRINT("INFO:    Detected PX4 Mavlink SYSID %d\n", _sysid);
    }

    switch(msg->msgid){
        case MAVLINK_MSG_ID_GPS_RAW_INT:
            {
                mavlink_gps_raw_int_t gps_raw_int;
                mavlink_msg_gps_raw_int_decode(msg, &gps_raw_int);
                _fix_type = gps_raw_int.fix_type;
            }
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_SYSTEM:
            {
                // Since we have operator location packets, use this instead 
                use_loc_for_operator_id = 0;
                _handle_operator_location_drone_id(msg);
                set_service_state_to_ready(MAVLINK);
            }
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_BASIC_ID:
            {
                // Currently not used since we can set this during the
                // uas_data initialization
            }
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_LOCATION:
            {
                _handle_location_drone_id(msg);
                set_service_state_to_ready(MAVLINK);
            }
            break;
        default:
            break;
    }
}


/// @brief MPA callback for handling mavlink packets
/// @param ch 
/// @param data 
/// @param bytes 
/// @param context 
static void _data_from_px4_helper_cb(__attribute__((unused))int ch, char* data, \
                                    int bytes, __attribute__((unused)) void* context) {
    // validate that the data makes sense
    int n_packets;
    mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
    if(msg_array == NULL){
        //pipe_client_flush(ch);
        return;
    }

    for(int i=0; i<n_packets; i++){
        mavlink_message_t msg = msg_array[i];
        _parse_px4_data(&msg);
    }

    return;
}

/// @brief MPA connect callback
/// @param ch 
/// @param context 
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context) {
    M_PRINT("INFO:    Connected to voxl-mavlink-server\n");
    return;
}


/// @brief MPA disconnect callback
/// @param ch 
/// @param context 
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context) {
    M_WARN("PX4 Mavlink Disconnected\n");
    
    set_service_state_to_not_ready(MAVLINK);
    return;
}

#define QRB5165_SN_BUF_LEN      12  // len 12
#define QRB5165_SN_BUF_LEN_CODE 'C' // len 1 (12 in hex)
#define CTA_2063_SN_LEN         (CTA_2063_MFG_ID_LEN + 1 + QRB5165_SN_BUF_LEN) // 4 + 1 + 12

/// @brief CTA-2063-A based serial number generation.
///        All UAS shall be assigned a unique Manufacturer’s Serial Number that becomes part of a UAS’ Serial Number (SN).
///        Each SN shall be comprised of three basic components: the Manufacturer Code, the Length Code, and the 
///        Manufacturer’s Serial Number.
/// @param sn - char array for SN
/// @param len - len of array
/// @return 0 on success
static int _generate_serial_number(char * sn, int len) {

    if(!sn){
        M_ERROR("invalid SN buffer");
        return -1;
    }
    if(len != CTA_2063_SN_LEN){
        M_ERROR("invalid SN buffer len");
        return -2;
    }
    memset(sn, 0x00, CTA_2063_SN_LEN);

    // Obtain platform serial number from device
    // TODO: this is QRB5165 only right now
    FILE *fd = fopen(SERIAL_NUMBER_PATH, "r");
    if (fd == NULL) {
        M_ERROR("Unable to open serial number file: %s\n", SERIAL_NUMBER_PATH);
        return -1;
    }

    char tmp[QRB5165_SN_BUF_LEN];
    memset(&tmp, 0x00, QRB5165_SN_BUF_LEN);
    fscanf(fd, "%s", tmp);
    fclose(fd);

    char mfg[4] = CTA_2063_MFG_ID;
    memcpy(&sn[0], &mfg[0], 4);
    char len_code = QRB5165_SN_BUF_LEN_CODE;
    memcpy(&sn[4], &len_code, 1);
    memcpy(&sn[5], &tmp, QRB5165_SN_BUF_LEN);

    M_PRINT("INFO:    Remote ID SN: %s\n", sn);

    return 0;
}

/// @brief ODID functions for packing and encoding data
/// @return 0 on success
static int _fill_uas_data() {

    char serial_num[CTA_2063_SN_LEN] = {};
    _generate_serial_number((char *)&serial_num, CTA_2063_SN_LEN);

    // Fill out basic ID
    uas_data.BasicID[BASIC_ID_POS_ZERO].UAType = ODID_UATYPE_HELICOPTER_OR_MULTIROTOR;
    uas_data.BasicID[BASIC_ID_POS_ZERO].IDType = ODID_IDTYPE_SERIAL_NUMBER;
    strncpy(uas_data.BasicID[BASIC_ID_POS_ZERO].UASID, serial_num, MINIMUM(sizeof(serial_num), sizeof(uas_data.BasicID[BASIC_ID_POS_ZERO].UASID)));

    // Basic ID only for POS 0, not POS 1
    uas_data.BasicID[BASIC_ID_POS_ONE].UAType = ODID_UATYPE_NONE;
    uas_data.BasicID[BASIC_ID_POS_ONE].IDType = ODID_IDTYPE_NONE;
    strncpy(uas_data.BasicID[BASIC_ID_POS_ONE].UASID, "", sizeof(uas_data.BasicID[BASIC_ID_POS_ONE].UASID));


    // For now keep auth data blank
    uas_data.Auth[0].AuthType = ODID_AUTH_NONE;
    uas_data.Auth[0].DataPage = 0;
    uas_data.Auth[0].LastPageIndex = 2;
    uas_data.Auth[0].Length = 63;
    uas_data.Auth[0].Timestamp = 28000000;
    char auth0_data[] = "";
    memcpy(uas_data.Auth[0].AuthData, auth0_data, MINIMUM(sizeof(auth0_data), sizeof(uas_data.Auth[0].AuthData)));

    uas_data.Auth[1].AuthType = ODID_AUTH_NONE;
    uas_data.Auth[1].DataPage = 1;
    char auth1_data[] = "";
    memcpy(uas_data.Auth[1].AuthData, auth1_data, MINIMUM(sizeof(auth1_data), sizeof(uas_data.Auth[1].AuthData)));

    uas_data.Auth[2].AuthType = ODID_AUTH_NONE;
    uas_data.Auth[2].DataPage = 2;
    char auth2_data[] = "";
    memcpy(uas_data.Auth[2].AuthData, auth2_data,  MINIMUM(sizeof(auth2_data), sizeof(uas_data.Auth[2].AuthData)));

    // General free-form information text
    uas_data.SelfID.DescType = ODID_DESC_TYPE_TEXT;

    // Drone Operators registration ID
    uas_data.OperatorID.OperatorIdType = ODID_OPERATOR_ID;

    // Read configured values from conf file
    int ret = read_config_file(&uas_data);
    if (ret < 0) {
        M_ERROR("Could not read from config file\n");
        return -1;
    }

    return 0;
}

/// @brief 
/// @param pack_enc ODID_MessagePack_encoded
static void _create_message_pack(struct ODID_MessagePack_encoded *pack_enc) {
    union ODID_Message_encoded encoded = { 0 };
    ODID_MessagePack_data pack_data = { 0 };
    pack_data.SingleMessageSize = ODID_MESSAGE_SIZE;
    pack_data.MsgPackSize = 9;
    
    if (encodeBasicIDMessage((ODID_BasicID_encoded *) &encoded, &uas_data.BasicID[BASIC_ID_POS_ZERO]) != ODID_SUCCESS)
        M_ERROR("Failed to encode Basic ID\n");
    memcpy(&pack_data.Messages[0], &encoded, ODID_MESSAGE_SIZE);

    if (encodeBasicIDMessage((ODID_BasicID_encoded *) &encoded, &uas_data.BasicID[BASIC_ID_POS_ONE]) != ODID_SUCCESS)
        M_ERROR("Failed to encode Basic ID\n");
    memcpy(&pack_data.Messages[1], &encoded, ODID_MESSAGE_SIZE);

    if (encodeLocationMessage((ODID_Location_encoded *) &encoded, &uas_data.Location) != ODID_SUCCESS)
        M_ERROR("Failed to encode Location\n");
    memcpy(&pack_data.Messages[2], &encoded, ODID_MESSAGE_SIZE);

    if (encodeAuthMessage((ODID_Auth_encoded *) &encoded, &uas_data.Auth[0]) != ODID_SUCCESS)
        M_ERROR("Failed to encode Auth 0\n");
    memcpy(&pack_data.Messages[3], &encoded, ODID_MESSAGE_SIZE);

    if (encodeAuthMessage((ODID_Auth_encoded *) &encoded, &uas_data.Auth[1]) != ODID_SUCCESS)
        M_ERROR("Failed to encode Auth 1\n");
    memcpy(&pack_data.Messages[4], &encoded, ODID_MESSAGE_SIZE);

    if (encodeAuthMessage((ODID_Auth_encoded *) &encoded, &uas_data.Auth[2]) != ODID_SUCCESS)
        M_ERROR("Failed to encode Auth 2\n");
    memcpy(&pack_data.Messages[5], &encoded, ODID_MESSAGE_SIZE);

    if (encodeSelfIDMessage((ODID_SelfID_encoded *) &encoded, &uas_data.SelfID) != ODID_SUCCESS)
        M_ERROR("Failed to encode Self ID\n");
    memcpy(&pack_data.Messages[6], &encoded, ODID_MESSAGE_SIZE);

    if (encodeSystemMessage((ODID_System_encoded *) &encoded, &uas_data.System) != ODID_SUCCESS)
        M_ERROR("Failed to encode System\n");
    memcpy(&pack_data.Messages[7], &encoded, ODID_MESSAGE_SIZE);

    if (encodeOperatorIDMessage((ODID_OperatorID_encoded *) &encoded, &uas_data.OperatorID) != ODID_SUCCESS)
        M_ERROR("Failed to encode Operator ID\n");
    memcpy(&pack_data.Messages[8], &encoded, ODID_MESSAGE_SIZE);

    if (encodeMessagePack(pack_enc, &pack_data) != ODID_SUCCESS)
        M_ERROR("Failed to encode message pack_data\n");
}

/// @brief 
/// @param msg_counters 
static void _send_packs(uint8_t *msg_counters) {
    struct ODID_MessagePack_encoded pack_enc = { 0 };

    _create_message_pack(&pack_enc);
    send_beacon_message_pack(&pack_enc, msg_counters[ODID_MSG_COUNTER_PACKED]++);
    
    M_DEBUG("DEBUG:   Message counter: %d\n", msg_counters[ODID_MSG_COUNTER_PACKED]);
}


/// @brief Thread responsible for sending beacon messages
/// @param ctx 
/// @return 
static void* _wifi_beacon_thread(__attribute__((unused)) void* ctx) {
    uint8_t msg_counters[ODID_MSG_COUNTER_AMOUNT] = { 0 };
    
    while (main_running) {
        _send_packs(msg_counters);

        // loop back to 0 once we reach the uint8_t limit
        if (msg_counters[ODID_MSG_COUNTER_PACKED] == 255) msg_counters[ODID_MSG_COUNTER_PACKED] = 0;
        sleep(4);
    }

    return NULL;
}

/// @brief Check if remote ID health is OK
/// @return 1 on success
static int _remote_id_health_ok(){

    //
    // TODO, these are MVP checks
    // - If we've seen drone_id mavlink packets
    // - if hostapd status is ok
    //
    if (uas_data.System.Timestamp == 0){
        M_VERBOSE("DEBUG:   Health: No MAVLink timestamp\n");
        return 0;
    }
    if (_remote_id_active == 0) {
        M_VERBOSE("DEBUG:   Health: MAVLink or SoftAP error\n");
        return 0;
    }

    // see https://mavlink.io/en/messages/common.html#GPS_FIX_TYPE
    if (_fix_type <= GPS_FIX_TYPE_NO_FIX) {
        M_VERBOSE("DEBUG:   Health: No GPS fix\n");
        return 0;
    }

    return 1;
}

/// @brief See https://mavlink.io/en/services/opendroneid.html#heartbeat
/// @param system_status MAV_STATE_ACTIVE or MAV_STATE_UNINIT
static void _send_heartbeat(uint8_t system_status){
    int ret = 0;

    M_VERBOSE("DEBUG:   Sending heartbeat\n");

    mavlink_message_t msg;
    mavlink_msg_heartbeat_pack(_sysid, MAV_COMP_ID_ODID_TXRX_1, &msg, \
            MAV_TYPE_ODID, MAV_AUTOPILOT_INVALID, 0, 0, system_status);

    ret = pipe_client_send_control_cmd_bytes(PX4_MAVLINK_CH, &msg, sizeof(mavlink_message_t));

    if (ret < 0) {
        M_ERROR("Failed to send heartbeat\n");
    }
}

/// @brief Thread responsible for updating and sending MAVLink heartbeat status for remote ID
/// @param ctx 
/// @return 
static void* _heartbeat_thread(__attribute__((unused)) void* ctx) {
    uint8_t system_status;
    while (main_running){
        system_status = _remote_id_health_ok() ? MAV_STATE_ACTIVE : MAV_STATE_UNINIT;
        _send_heartbeat(system_status);
        sleep(MSG_INT_SEC_HEARTBEAT_MSG);
    }

    return NULL;
}

/// @brief Main
/// @param argc 
/// @param argv 
/// @return 
int main(int argc, char *argv[]) {
    int ret;

    ret = _parse_args(argc, argv);
    if (ret < 0){
        return ret;
    }

////////////////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////////////////

    odid_initUasData(&uas_data);
    if (_fill_uas_data() < 0) {
        M_ERROR("Failed to fill uas data\n");
        return -1;
    }

    pthread_attr_t tattr;
    pthread_attr_init(&tattr);
    sem_init(&hostapd_sem, 0, 0);

    main_running = 1;

    pthread_create(&hostapd_thread, &tattr, hostapd_init_thread, NULL);
    sem_wait(&hostapd_sem);
    pthread_create(&beacon_thread, &tattr, _wifi_beacon_thread, NULL);

    // open the pipe from voxl-mavlink-server
    pipe_client_set_connect_cb(PX4_MAVLINK_CH, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(PX4_MAVLINK_CH, _disconnect_cb, NULL);
	pipe_client_set_simple_helper_cb(PX4_MAVLINK_CH, _data_from_px4_helper_cb, NULL);
    pipe_client_open(PX4_MAVLINK_CH, PX4_MAVLINK_PIPE_NAME, PIPE_CLIENT_NAME,
        CLIENT_FLAG_EN_SIMPLE_HELPER,\
        MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);

    pthread_create(&heartbeat_thread, &tattr, _heartbeat_thread, NULL);

////////////////////////////////////////////////////////////////////////////////
// Start
////////////////////////////////////////////////////////////////////////////////
    while (main_running) usleep(50000);

////////////////////////////////////////////////////////////////////////////////
// Cleanup
////////////////////////////////////////////////////////////////////////////////

    send_quit();
    pthread_join(hostapd_thread, NULL);
    pthread_join(beacon_thread, NULL);
    sem_destroy(&hostapd_sem);
    
    return 0;
}