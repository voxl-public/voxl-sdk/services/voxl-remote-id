/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h> // free()

// Local Includes
#include "config_file.h"

// ModalAI includes
#include <modal_json.h>
#include <modal_journal.h>


// Json meta
#define CONFIG_FILE_NAME    "/etc/modalai/voxl-remote-id.conf"
#define CONFIG_FILE_VERSION 0.1

// Default values
#define SELFID_DEFAULT      "ModalAI Drone Flight"
#define OPERATOR_ID_DEFAULT "N/A"

// Elements of the configuration file
#define JsonVersionString    "version"           // Config file version
#define JsonOperatorIDString "operator_id"       // Drone operator's registration ID
#define JsonSelfIDString     "self_id_message"   // Self ID text


// -----------------------------------------------------------------------------------------------------------------------------
// Reads values from config file
// -----------------------------------------------------------------------------------------------------------------------------
int read_config_file(struct ODID_UAS_Data *uas_data) {
    cJSON* head = json_read_file(CONFIG_FILE_NAME);

    if(json_fetch_string(head, JsonSelfIDString, uas_data->SelfID.Desc, sizeof(uas_data->SelfID.Desc))) {
        M_ERROR("Could not read in self ID string\n");
        return -1;
    }
    if(json_fetch_string(head, JsonOperatorIDString, uas_data->OperatorID.OperatorId, sizeof(uas_data->OperatorID.OperatorId))) {
        M_ERROR("Could not read in self ID string\n");
        return -1;
    }

    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Generates a new factory default JSON file
// -----------------------------------------------------------------------------------------------------------------------------
int generate_config_file() {
    cJSON* head = cJSON_CreateObject();
    cJSON_AddNumberToObject(head, JsonVersionString, CONFIG_FILE_VERSION);
    cJSON_AddStringToObject(head, JsonOperatorIDString, OPERATOR_ID_DEFAULT);
    cJSON_AddStringToObject(head, JsonSelfIDString, SELFID_DEFAULT);

    FILE *file = fopen(CONFIG_FILE_NAME, "w");
    if (file == NULL) {
        M_ERROR("Could not open config file for writing: %s\n", CONFIG_FILE_NAME);
        return -1;
    }
    else {
        M_PRINT("INFO:    Writing new configuration file to %s\n", CONFIG_FILE_NAME);
        char *jsonString = cJSON_Print(head);
        fwrite(jsonString, 1, strlen(jsonString), file);

        fclose(file);
        free(jsonString);
    }

    cJSON_Delete(head);
    return 0;
}