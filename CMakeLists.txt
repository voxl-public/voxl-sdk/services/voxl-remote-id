#******************************************************************************
# Copyright 2022 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#*****************************************************************************/

cmake_minimum_required(VERSION 3.3)

project(voxl-remote-id)

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DCONFIG_CTRL_IFACE -DCONFIG_CTRL_IFACE_UNIX")


# James' standard list of cmake flags
set(CMAKE_C_FLAGS "-std=gnu99 -Wno-unused-parameter -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion -Wmissing-prototypes \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_C_FLAGS}")

set(CMAKE_CXX_FLAGS "-std=gnu++11 -std=c++11 -Wno-unused-parameter -Wall -Wextra -Wuninitialized -Wno-attributes \
    -Wunused-variable -Wno-double-promotion -Wno-missing-field-initializers \
    -Werror=undef -Wno-unused-function ${CMAKE_CXX_FLAGS}")

# for VOXL, install 64-bit libraries to lib64
set(LIB_INSTALL_DIR /usr/lib64)

set(TARGET voxl-remote-id)

if(PLATFORM MATCHES APQ8096)
    # dual-architecture libs, point to the 64-bit one
    find_library(MODAL_JSON    modal_json    HINTS  /usr/lib)
    find_library(MODAL_PIPE    modal_pipe    HINTS  /usr/lib)
    find_library(MODAL_JOURNAL modal_journal HINTS  /usr/lib)
else()
    # dual-architecture libs, point to the 64-bit one
    find_library(MODAL_JSON    modal_json    HINTS  /usr/lib64)
    find_library(MODAL_PIPE    modal_pipe    HINTS  /usr/lib64)
    find_library(MODAL_JOURNAL modal_journal HINTS  /usr/lib64)
endif()

include_directories(
        include/
        hostapd/src
        hostapd/src/utils
        opendroneid-core-c/libopendroneid
)

# Hostapd files required for interactive mode
set(HOSTAPD_FILES
    "hostapd/src/common/cli.c"
    "hostapd/src/utils/edit.c"
    "hostapd/src/utils/eloop.c"
    "hostapd/src/utils/common.c"
    "hostapd/src/utils/os_unix.c"
    "hostapd/src/utils/wpa_debug.c"
    "hostapd/src/common/wpa_ctrl.c" 
)

# OpenDroneID API
set(OPENDRONEID_FILES
    "opendroneid-core-c/libopendroneid/opendroneid.c"
)

file(GLOB_RECURSE SRC_FILES "src/*.c*")
add_executable(${TARGET}
        ${HOSTAPD_FILES}
        ${OPENDRONEID_FILES}
        ${SRC_FILES}
)

target_link_libraries(${TARGET}
    m    
    pthread
    ${MODAL_JSON}
    ${MODAL_PIPE}
    ${MODAL_JOURNAL}
)

install(
    TARGETS         ${TARGET}
    LIBRARY         DESTINATION /usr/lib
    RUNTIME         DESTINATION /usr/bin
    PUBLIC_HEADER   DESTINATION /usr/include
)
