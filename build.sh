#!/bin/bash
################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
################################################################################


## this list is just for tab-completion
AVAILABLE_PLATFORMS="qrb5165 apq8096"


print_usage(){
    echo ""
    echo " Build the current project based on platform target."
    echo ""
    echo " Usage:"
    echo ""
    echo "  ./build.sh apq8096"
    echo "        Build 64-bit binaries for apq8096"
    echo "        use in voxl-emulator docker image"
    echo ""
    echo "  ./build.sh qrb5165"
    echo "        Build 64-bit binaries for qrb5165"
    echo "        use in qrb5165-emulator docker image"
    echo ""
    echo ""
}


case "$1" in
    apq8096)
        # Apply patches for submodules
        cd hostapd || exit 1
        cp ../patches/clean-build-warnings.patch .
        patch -p1 < clean-build-warnings.patch
        cd ../
        ## single-arch library, build only 32 bit
        mkdir -p build
        cd build
        cmake -DPLATFORM=APQ8096 ${EXTRA_OPTS} ../
        make -j$(nproc)
        cd ../
        ;;
    qrb5165)
        # Apply patches for submodules
        patch -p1 -uN -d hostapd -i ../patches/clean-build-warnings.patch
        ## single-arch library, build just 64
        mkdir -p build
        cd build
        cmake -DPLATFORM=QRB5165 ${EXTRA_OPTS} ../
        make -j$(nproc)
        cd ../
        ;;

    *)
        print_usage
        exit 1
        ;;
esac
