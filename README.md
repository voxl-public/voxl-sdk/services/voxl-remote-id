# voxl-remote-id

## Build dependencies

- libmodal-json
- libmodal-pipe
- voxl-mavlink
- libmodal-journal

## Build Environment

This project builds in the voxl-emulator and qrb5165-emulator docker images

Follow the instructions here to build and install the qrb5165-emulator docker image:
https://gitlab.com/voxl-public/voxl-docker


## Build Instructions

1) Pull in the hostapd and opendroneid-core-c submodules
```bash
git submodule update --init --recursive
```

2) Launch the right emulator docker for the platform you are building for

```bash
~/git/voxl-remote-id$ voxl-docker -i voxl-emulator
voxl-emulator:~$
OR
~/git/voxl-remote-id$ voxl-docker -i qrb5165-emulator
qrb5165-emulator:~$
```

3) Install dependencies inside the docker. You must specify both the hardware platform and binary repo section to pull from. CI will use the `dev` binary repo for `dev` branch jobs, otherwise it will select the correct target SDK-release based on tags. When building yourself, you must decide what your intended target is, usually `dev` or `staging`

```bash
qrb5165-emulator:~$ ./install_build_deps.sh qrb5165 dev
OR
voxl-emulator:~$ ./install_build_deps.sh voxl dev
```


4) Build scripts should take the hardware platform as an argument: `qrb5165` or `apq8096`. CI will pass these arguments to the build script based on the job target.

```bash
qrb5165-emulator:~$ ./build.sh qrb5165
OR
voxl-emulator:~$ ./build.sh apq8096
```


5) Make an ipk or deb package while still inside the docker.

```bash
qrb5165-emulator:~$ ./make_package.sh deb
OR
voxl-emulator:~$ ./make_package.sh ipk
```

This will make a new package file in your working directory. The name and version number came from the package control file. If you are updating the package version, edit it there.

Optionally add the --timestamp argument to append the current data and time to the package version number in the debian package. This is done automatically by the CI package builder for development and nightly builds, however you can use it yourself if you like.


## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
voxl-remote-id$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.


```bash
(outside of docker)
voxl-remote-id$ ./deploy_to_voxl.sh ssh 192.168.1.123
```


